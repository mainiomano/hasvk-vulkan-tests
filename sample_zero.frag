#version 450

layout(set = 0, binding = 0) uniform sampler2DMS image;
layout(location = 0) out vec4 outColor;

void main() {
    outColor = texelFetch(image, ivec2(gl_FragCoord.xy), 0);
}
