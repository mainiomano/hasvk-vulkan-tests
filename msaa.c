#include "volk.h"
#include <png.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

struct Vertex {
  float x;
  float y;
};

struct RGBA8888 {
  uint8_t r;
  uint8_t g;
  uint8_t b;
  uint8_t a;
};

struct RGB888 {
  uint8_t r;
  uint8_t g;
  uint8_t b;
};

static const struct Vertex triangle[] = {
    {-0.5f, 0.5f},
    {0.5f, 0.5f},
    {0.f, -0.5f},
};

static const struct Vertex full_rectangle[] = {
    {-1.f, -1.f}, {-1.f, 1.f}, {1.f, 1.f},
    {-1.f, -1.f}, {1.f, 1.f},  {1.f, -1.f},
};

static VkShaderModule create_shader_module(VkDevice device,
                                           const char *file_name) {
  FILE *file = fopen(file_name, "r");
  uint32_t shader_buf[1024];
  size_t length = fread(shader_buf, sizeof(uint32_t), 1024, file);
  fclose(file);
  VkShaderModuleCreateInfo shader_module_create_info = {
      .sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO,
      .pNext = NULL,
      .flags = 0,
      .codeSize = length * sizeof(uint32_t),
      .pCode = shader_buf,
  };
  VkShaderModule shader_module;
  vkCreateShaderModule(device, &shader_module_create_info, NULL,
                       &shader_module);
  return shader_module;
}

/* Create a 64x64 RGBA8888 image with the given number of samples and allocate
 * memory for it, */
static VkImage create_image(VkDevice device, VkImageUsageFlags usage,
                            VkSampleCountFlagBits samples) {
  VkImageCreateInfo image_create_info = {
      .sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO,
      .pNext = NULL,
      .flags = 0,
      .imageType = VK_IMAGE_TYPE_2D,
      .format = VK_FORMAT_R8G8B8A8_UNORM,
      .extent =
          (VkExtent3D){
              .width = 64,
              .height = 64,
              .depth = 1,
          },
      .mipLevels = 1,
      .arrayLayers = 1,
      .samples = samples,
      .tiling = VK_IMAGE_TILING_OPTIMAL,
      .usage = usage,
      .sharingMode = VK_SHARING_MODE_EXCLUSIVE,
      .queueFamilyIndexCount = 0,
      .pQueueFamilyIndices = NULL,
      .initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
  };
  VkImage image;
  vkCreateImage(device, &image_create_info, NULL, &image);
  VkMemoryRequirements memory_requirements;
  vkGetImageMemoryRequirements(device, image, &memory_requirements);
  VkMemoryAllocateInfo allocate_info = {
      .sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
      .pNext = NULL,
      .allocationSize = memory_requirements.size,
      .memoryTypeIndex = 0,
  };
  VkDeviceMemory memory;
  vkAllocateMemory(device, &allocate_info, NULL, &memory);
  vkBindImageMemory(device, image, memory, 0);

  return image;
}

static void initial_transition_image(VkCommandBuffer cb, VkImage image,
                                     VkAccessFlags access_flags,
                                     VkPipelineStageFlags stages,
                                     VkImageLayout target_layout) {
  const VkImageMemoryBarrier image_memory_barrier = {
      .sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
      .pNext = NULL,
      .srcAccessMask = 0,
      .dstAccessMask = access_flags,
      .oldLayout = VK_IMAGE_LAYOUT_UNDEFINED,
      .newLayout = target_layout,
      .srcQueueFamilyIndex = 0,
      .dstQueueFamilyIndex = 0,
      .image = image,
      .subresourceRange =
          {
              .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
              .baseMipLevel = 0,
              .levelCount = 1,
              .baseArrayLayer = 0,
              .layerCount = 1,
          },
  };
  vkCmdPipelineBarrier(cb, VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, stages, 0, 0,
                       NULL, 0, NULL, 1, &image_memory_barrier);
}

static VkImageView create_image_view(VkDevice device, VkImage image) {
  const VkImageViewCreateInfo image_view_create_info = {
      .sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
      .pNext = NULL,
      .flags = 0,
      .image = image,
      .viewType = VK_IMAGE_VIEW_TYPE_2D,
      .format = VK_FORMAT_R8G8B8A8_UNORM,
      .components =
          {
              VK_COMPONENT_SWIZZLE_IDENTITY,
              VK_COMPONENT_SWIZZLE_IDENTITY,
              VK_COMPONENT_SWIZZLE_IDENTITY,
              VK_COMPONENT_SWIZZLE_IDENTITY,
          },
      .subresourceRange =
          {
              .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
              .baseMipLevel = 0,
              .levelCount = 1,
              .baseArrayLayer = 0,
              .layerCount = 1,
          },
  };
  VkImageView image_view;
  vkCreateImageView(device, &image_view_create_info, NULL, &image_view);
  return image_view;
}

static VkBuffer create_buffer(VkDevice device, size_t size,
                              VkBufferUsageFlags usage,
                              VkDeviceMemory *buffer_memory) {
  VkBufferCreateInfo buffer_create_info = {
      .sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
      .pNext = NULL,
      .flags = 0,
      .size = size,
      .usage = usage,
      .sharingMode = VK_SHARING_MODE_EXCLUSIVE,
      .queueFamilyIndexCount = 0,
      .pQueueFamilyIndices = NULL,
  };
  VkBuffer buffer;
  vkCreateBuffer(device, &buffer_create_info, NULL, &buffer);
  VkMemoryRequirements buffer_memory_requirements;
  vkGetBufferMemoryRequirements(device, buffer, &buffer_memory_requirements);
  const VkMemoryAllocateInfo buffer_allocate_info = {
      .sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
      .pNext = NULL,
      .allocationSize = buffer_memory_requirements.size,
      .memoryTypeIndex = 0,
  };
  vkAllocateMemory(device, &buffer_allocate_info, NULL, buffer_memory);
  vkBindBufferMemory(device, buffer, *buffer_memory, 0);
  return buffer;
}

static VkBuffer create_vertex_buffer(VkDevice device, size_t vertex_count,
                                     const struct Vertex *vertices) {
  struct Vertex *mapped_memory;
  VkDeviceMemory vertex_buffer_memory;
  VkBuffer vertex_buffer =
      create_buffer(device, vertex_count * sizeof *vertices,
                    VK_BUFFER_USAGE_VERTEX_BUFFER_BIT, &vertex_buffer_memory);
  vkMapMemory(device, vertex_buffer_memory, 0, VK_WHOLE_SIZE, 0,
              (void **)&mapped_memory);
  memcpy(mapped_memory, vertices, vertex_count * sizeof *vertices);
  vkUnmapMemory(device, vertex_buffer_memory);
  return vertex_buffer;
}

/* The image's layout must be VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL. */
static void copy_image_to_buffer(VkCommandBuffer cb, VkImage image,
                                 VkBuffer buffer, VkDeviceSize offset) {
  const VkBufferImageCopy copy_regions = {
      .bufferOffset = offset,
      .bufferRowLength = 0,
      .bufferImageHeight = 0,
      .imageSubresource =
          {
              .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
              .mipLevel = 0,
              .baseArrayLayer = 0,
              .layerCount = 1,
          },
      .imageOffset =
          {
              .x = 0,
              .y = 0,
              .z = 0,
          },
      .imageExtent =
          {
              .width = 64,
              .height = 64,
              .depth = 1,
          },
  };
  vkCmdCopyImageToBuffer(cb, image, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
                         buffer, 1, &copy_regions);
}

static void write_image_to_file(VkDevice device, VkDeviceMemory buffer_memory,
                                size_t pixel_offset, const char *filename) {
  struct RGBA8888 *mapped_buffer;
  vkMapMemory(device, buffer_memory, 0, VK_WHOLE_SIZE, 0,
              (void **)&mapped_buffer);
  png_image settings = {
      .version = PNG_IMAGE_VERSION,
      .width = 64,
      .height = 64,
      .format = PNG_FORMAT_RGBA,
  };
  png_image_write_to_file(&settings, filename, 0, mapped_buffer + pixel_offset,
                          0, NULL);
  vkUnmapMemory(device, buffer_memory);
}

static VkPipeline create_graphics_pipeline(VkDevice device,
                                           VkShaderModule vertex_shader,
                                           VkShaderModule fragment_shader,
                                           VkPipelineLayout pipeline_layout,
                                           VkSampleCountFlagBits samples,
                                           VkBool32 sample_shading) {
  const VkPipelineShaderStageCreateInfo pipeline_stages[] = {
      {
          .sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
          .pNext = NULL,
          .flags = 0,
          .stage = VK_SHADER_STAGE_VERTEX_BIT,
          .module = vertex_shader,
          .pName = "main",
          .pSpecializationInfo = NULL,
      },
      {
          .sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
          .pNext = NULL,
          .flags = 0,
          .stage = VK_SHADER_STAGE_FRAGMENT_BIT,
          .module = fragment_shader,
          .pName = "main",
          .pSpecializationInfo = NULL,
      },
  };
  const VkVertexInputBindingDescription vertex_input_bindings = {
      .binding = 0,
      .stride = sizeof(struct Vertex),
      .inputRate = VK_VERTEX_INPUT_RATE_VERTEX,
  };
  const VkVertexInputAttributeDescription vertex_input_attributes = {
      .location = 0,
      .binding = 0,
      .format = VK_FORMAT_R32G32_SFLOAT,
      .offset = 0,
  };
  const VkPipelineVertexInputStateCreateInfo vertex_input_state = {
      .sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO,
      .pNext = NULL,
      .flags = 0,
      .vertexBindingDescriptionCount = 1,
      .pVertexBindingDescriptions = &vertex_input_bindings,
      .vertexAttributeDescriptionCount = 1,
      .pVertexAttributeDescriptions = &vertex_input_attributes,
  };
  const VkPipelineInputAssemblyStateCreateInfo input_assembly_state = {
      .sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO,
      .pNext = NULL,
      .flags = 0,
      .topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST,
      .primitiveRestartEnable = VK_FALSE,
  };
  const VkViewport viewport = {
      .x = 0.f,
      .y = 0.f,
      .width = 64.f,
      .height = 64.f,
      .minDepth = 0.f,
      .maxDepth = 1.f,
  };
  const VkRect2D scissor_rect = {
      .extent =
          {
              .width = 64,
              .height = 64,
          },
      .offset =
          {
              .x = 0,
              .y = 0,
          },
  };
  const VkPipelineViewportStateCreateInfo viewport_state = {
      .sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO,
      .pNext = NULL,
      .flags = 0,
      .viewportCount = 1,
      .pViewports = &viewport,
      .scissorCount = 1,
      .pScissors = &scissor_rect,
  };
  const VkPipelineRasterizationStateCreateInfo rasterization_state = {
      .sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO,
      .pNext = NULL,
      .flags = 0,
      .depthClampEnable = VK_FALSE,
      .rasterizerDiscardEnable = VK_FALSE,
      .polygonMode = VK_POLYGON_MODE_FILL,
      .cullMode = VK_CULL_MODE_NONE,
      .frontFace = VK_FRONT_FACE_CLOCKWISE,
      .depthBiasEnable = VK_FALSE,
      .depthBiasConstantFactor = 0.f,
      .depthBiasClamp = 0.f,
      .depthBiasSlopeFactor = 0.f,
      .lineWidth = 1.f,
  };
  const VkPipelineMultisampleStateCreateInfo multisample_state = {
      .sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO,
      .pNext = NULL,
      .flags = 0,
      .rasterizationSamples = samples,
      .sampleShadingEnable = sample_shading,
      .minSampleShading = 1.f,
      .pSampleMask = NULL,
      .alphaToCoverageEnable = VK_FALSE,
      .alphaToOneEnable = VK_FALSE,
  };
  const VkPipelineColorBlendAttachmentState blend_attachment_state = {
      .blendEnable = VK_FALSE,
      .srcColorBlendFactor = VK_BLEND_FACTOR_ONE,
      .dstColorBlendFactor = VK_BLEND_FACTOR_ZERO,
      .colorBlendOp = VK_BLEND_OP_ADD,
      .srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE,
      .dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO,
      .alphaBlendOp = VK_BLEND_OP_ADD,
      .colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT |
                        VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT,
  };
  const VkPipelineColorBlendStateCreateInfo blend_state = {
      .sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO,
      .pNext = NULL,
      .logicOpEnable = VK_FALSE,
      .logicOp = VK_LOGIC_OP_NO_OP,
      .attachmentCount = 1,
      .pAttachments = &blend_attachment_state,
      .blendConstants = {0},
  };
  const VkPipelineRenderingCreateInfoKHR pipeline_rendering_create_info = {
      .sType = VK_STRUCTURE_TYPE_PIPELINE_RENDERING_CREATE_INFO_KHR,
      .pNext = NULL,
      .viewMask = 0,
      .colorAttachmentCount = 1,
      .pColorAttachmentFormats = &(VkFormat){VK_FORMAT_R8G8B8A8_UNORM},
      .depthAttachmentFormat = VK_FORMAT_UNDEFINED,
      .stencilAttachmentFormat = VK_FORMAT_UNDEFINED,
  };
  const VkGraphicsPipelineCreateInfo graphics_pipeline_create_info = {
      .sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO,
      .pNext = &pipeline_rendering_create_info,
      .flags = 0,
      .stageCount = sizeof pipeline_stages / sizeof pipeline_stages[0],
      .pStages = pipeline_stages,
      .pVertexInputState = &vertex_input_state,
      .pInputAssemblyState = &input_assembly_state,
      .pTessellationState = NULL,
      .pViewportState = &viewport_state,
      .pRasterizationState = &rasterization_state,
      .pMultisampleState = &multisample_state,
      .pDepthStencilState = NULL,
      .pColorBlendState = &blend_state,
      .pDynamicState = NULL,
      .layout = pipeline_layout,
      .renderPass = VK_NULL_HANDLE,
      .subpass = 0,
      .basePipelineHandle = VK_NULL_HANDLE,
      .basePipelineIndex = 0,
  };
  VkPipeline pipeline;
  vkCreateGraphicsPipelines(device, VK_NULL_HANDLE, 1,
                            &graphics_pipeline_create_info, NULL, &pipeline);
  return pipeline;
}

static void begin_rendering(VkCommandBuffer cb, VkImageView target) {
  const VkRenderingAttachmentInfoKHR attachment_info = {
      .sType = VK_STRUCTURE_TYPE_RENDERING_ATTACHMENT_INFO_KHR,
      .pNext = NULL,
      .imageView = target,
      .imageLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
      .resolveMode = VK_RESOLVE_MODE_NONE,
      .resolveImageView = VK_NULL_HANDLE,
      .resolveImageLayout = 0,
      .loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR,
      .storeOp = VK_ATTACHMENT_STORE_OP_STORE,
      .clearValue = {.color = {.float32 = {0.f, 1.f, 0.f, 1.f}}},
  };
  const VkRenderingInfoKHR rendering_info = {
      .sType = VK_STRUCTURE_TYPE_RENDERING_INFO_KHR,
      .pNext = NULL,
      .flags = 0,
      .renderArea = {.extent = {.width = 64, .height = 64},
                     .offset = {.x = 0, .y = 0}},
      .layerCount = 1,
      .viewMask = 0,
      .colorAttachmentCount = 1,
      .pColorAttachments = &attachment_info,
      .pDepthAttachment = NULL,
      .pStencilAttachment = NULL,
  };
  vkCmdBeginRenderingKHR(cb, &rendering_info);
}

int main(void) {
  volkInitialize();
  const VkApplicationInfo app_info = {
      .sType = VK_STRUCTURE_TYPE_APPLICATION_INFO,
      .pNext = NULL,
      .pApplicationName = NULL,
      .applicationVersion = VK_MAKE_VERSION(0, 0, 0),
      .pEngineName = NULL,
      .engineVersion = VK_MAKE_VERSION(0, 0, 0),
      .apiVersion = VK_API_VERSION_1_2,
  };
  const VkInstanceCreateInfo instance_create_info = {
      .sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO,
      .pNext = NULL,
      .flags = 0,
      .pApplicationInfo = &app_info,
      .enabledLayerCount = 0,
      .ppEnabledLayerNames = NULL,
      .enabledExtensionCount = 0,
      .ppEnabledExtensionNames = NULL,
  };
  VkInstance instance;
  vkCreateInstance(&instance_create_info, NULL, &instance);
  volkLoadInstanceOnly(instance);

  VkPhysicalDevice physical_device;
  uint32_t physical_device_count = 1;
  vkEnumeratePhysicalDevices(instance, &physical_device_count,
                             &physical_device);

  const float queue_priorities = 1.0f;
  const VkDeviceQueueCreateInfo queue_create_info = {
      .sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
      .pNext = NULL,
      .flags = 0,
      .queueFamilyIndex = 0,
      .queueCount = 1,
      .pQueuePriorities = &queue_priorities,
  };
  const char *const enabled_extensions[] = {
      VK_KHR_DYNAMIC_RENDERING_EXTENSION_NAME};
  const VkPhysicalDeviceDynamicRenderingFeaturesKHR dynamic_rendering_features =
      {
          .sType =
              VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DYNAMIC_RENDERING_FEATURES_KHR,
          .pNext = NULL,
          .dynamicRendering = VK_TRUE,
      };
  const VkPhysicalDeviceFeatures enabled_features = {
      .sampleRateShading = VK_TRUE,
  };
  const VkDeviceCreateInfo device_create_info = {
      .sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO,
      .pNext = &dynamic_rendering_features,
      .flags = 0,
      .queueCreateInfoCount = 1,
      .pQueueCreateInfos = &queue_create_info,
      .enabledLayerCount = 0,
      .ppEnabledLayerNames = NULL,
      .enabledExtensionCount =
          sizeof enabled_extensions / sizeof enabled_extensions[0],
      .ppEnabledExtensionNames = enabled_extensions,
      .pEnabledFeatures = &enabled_features,
  };
  VkDevice device;
  vkCreateDevice(physical_device, &device_create_info, NULL, &device);
  volkLoadDevice(device);

  VkQueue queue;
  vkGetDeviceQueue(device, 0, 0, &queue);

  const VkCommandPoolCreateInfo command_pool_create_info = {
      .sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO,
      .pNext = NULL,
      .flags = 0,
      .queueFamilyIndex = 0,
  };
  VkCommandPool command_pool;
  vkCreateCommandPool(device, &command_pool_create_info, NULL, &command_pool);
  const VkCommandBufferAllocateInfo cb_allocate_info = {
      .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
      .pNext = NULL,
      .commandPool = command_pool,
      .level = VK_COMMAND_BUFFER_LEVEL_PRIMARY,
      .commandBufferCount = 1,
  };
  VkCommandBuffer cb;
  vkAllocateCommandBuffers(device, &cb_allocate_info, &cb);

  /* The image the graphics are rendered into */
  VkImage ms_image = create_image(device,
                                  VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT |
                                      VK_IMAGE_USAGE_SAMPLED_BIT |
                                      VK_IMAGE_USAGE_TRANSFER_SRC_BIT,
                                  VK_SAMPLE_COUNT_8_BIT);
  VkImageView ms_image_view = create_image_view(device, ms_image);
  /* A copy of ms_image */
  VkImage ms_image_copy = create_image(
      device, VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
      VK_SAMPLE_COUNT_8_BIT);
  VkImageView ms_image_copy_view = create_image_view(device, ms_image_copy);
  /* The image where ms_image is resolved to */
  VkImage resolved_image = create_image(
      device, VK_IMAGE_USAGE_TRANSFER_SRC_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT,
      VK_SAMPLE_COUNT_1_BIT);
  /* The image where the sample zero from ms_image is put  */
  VkImage sample_zero_image = create_image(device,
                                           VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT |
                                               VK_IMAGE_USAGE_TRANSFER_SRC_BIT,
                                           VK_SAMPLE_COUNT_1_BIT);
  VkImageView sample_zero_image_view =
      create_image_view(device, sample_zero_image);
  /* The iamge where the sample zero from ms_image_copy is put */
  VkImage sample_zero_copy_image = create_image(
      device,
      VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT,
      VK_SAMPLE_COUNT_1_BIT);
  VkImageView sample_zero_copy_image_view =
      create_image_view(device, sample_zero_copy_image);

  const VkShaderModule basic_vertex_shader =
      create_shader_module(device, "basic.vert.spv");
  const VkShaderModule ms_fragment_shader =
      create_shader_module(device, "msaa.frag.spv");
  VkPipelineLayoutCreateInfo ms_pipeline_layout_create_info = {
      .sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO,
      .flags = 0,
      .setLayoutCount = 0,
      .pSetLayouts = NULL,
      .pushConstantRangeCount = 0,
      .pPushConstantRanges = NULL,
  };
  VkPipelineLayout ms_pipeline_layout;
  vkCreatePipelineLayout(device, &ms_pipeline_layout_create_info, NULL,
                         &ms_pipeline_layout);
  const VkPipeline msaa_pipeline = create_graphics_pipeline(
      device, basic_vertex_shader, ms_fragment_shader, ms_pipeline_layout,
      VK_SAMPLE_COUNT_8_BIT, VK_FALSE);
  const VkShaderModule white_triangle_fragment_shader =
      create_shader_module(device, "white.frag.spv");
  const VkPipeline white_triangle_pipeline = create_graphics_pipeline(
      device, basic_vertex_shader, white_triangle_fragment_shader,
      ms_pipeline_layout, VK_SAMPLE_COUNT_8_BIT, VK_TRUE);

  const VkShaderModule sample_zero_fragment_shader =
      create_shader_module(device, "sample_zero.frag.spv");
  const VkDescriptorSetLayoutBinding sample_zero_descriptor_set_layout_binding =
      {
          .binding = 0,
          .descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
          .descriptorCount = 1,
          .stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT,
          .pImmutableSamplers = NULL,
      };
  const VkDescriptorSetLayoutCreateInfo
      sample_zero_descriptor_set_layout_create_info = {
          .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO,
          .pNext = NULL,
          .flags = 0,
          .bindingCount = 1,
          .pBindings = &sample_zero_descriptor_set_layout_binding,
      };
  VkDescriptorSetLayout sample_zero_descriptor_set_layout;
  vkCreateDescriptorSetLayout(device,
                              &sample_zero_descriptor_set_layout_create_info,
                              NULL, &sample_zero_descriptor_set_layout);
  const VkPipelineLayoutCreateInfo resolve_pipeline_layout_create_info = {
      .sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO,
      .flags = 0,
      .setLayoutCount = 1,
      .pSetLayouts = &sample_zero_descriptor_set_layout,
      .pushConstantRangeCount = 0,
      .pPushConstantRanges = NULL,
  };
  VkPipelineLayout sample_zero_pipeline_layout;
  vkCreatePipelineLayout(device, &resolve_pipeline_layout_create_info, NULL,
                         &sample_zero_pipeline_layout);
  const VkPipeline sample_zero_pipeline = create_graphics_pipeline(
      device, basic_vertex_shader, sample_zero_fragment_shader,
      sample_zero_pipeline_layout, VK_SAMPLE_COUNT_1_BIT, VK_FALSE);

  /* ms_image and ms_image_copy have their own descriptor sets, the first of
   * which is bound for creating sample_zero_image and the second for creating
   * sample_zero_copy_image */
  const VkDescriptorPoolSize descriptor_pool_size = {
      .type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
      .descriptorCount = 2,
  };
  const VkDescriptorPoolCreateInfo descriptor_pool_create_info = {
      .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO,
      .pNext = NULL,
      .flags = 0,
      .maxSets = 2,
      .poolSizeCount = 1,
      .pPoolSizes = &descriptor_pool_size,
  };
  VkDescriptorPool descriptor_pool;
  vkCreateDescriptorPool(device, &descriptor_pool_create_info, NULL,
                         &descriptor_pool);
  const VkDescriptorSetAllocateInfo sample_zero_descriptor_set_allocate_info = {
      .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO,
      .pNext = NULL,
      .descriptorPool = descriptor_pool,
      .descriptorSetCount = 2,
      .pSetLayouts =
          (VkDescriptorSetLayout[]){sample_zero_descriptor_set_layout,
                                    sample_zero_descriptor_set_layout},
  };
  VkDescriptorSet sample_zero_descriptor_sets[2];
  vkAllocateDescriptorSets(device, &sample_zero_descriptor_set_allocate_info,
                           sample_zero_descriptor_sets);

  VkSamplerCreateInfo sampler_create_info = {
      .sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO,
      .pNext = NULL,
      .flags = 0,
      .magFilter = VK_FILTER_NEAREST,
      .minFilter = VK_FILTER_NEAREST,
      .mipmapMode = VK_SAMPLER_MIPMAP_MODE_NEAREST,
      .addressModeU = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_BORDER,
      .addressModeV = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_BORDER,
      .addressModeW = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_BORDER,
      .mipLodBias = 0.f,
      .anisotropyEnable = VK_FALSE,
      .maxAnisotropy = 0.f,
      .compareEnable = VK_FALSE,
      .compareOp = VK_COMPARE_OP_ALWAYS,
      .minLod = 0.f,
      .maxLod = 0.f,
      .borderColor = VK_BORDER_COLOR_FLOAT_OPAQUE_BLACK,
      .unnormalizedCoordinates = VK_FALSE,
  };
  VkSampler sampler;
  vkCreateSampler(device, &sampler_create_info, NULL, &sampler);
  const VkDescriptorImageInfo sample_zero_image_info = {
      .sampler = sampler,
      .imageView = ms_image_view,
      .imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
  };
  const VkDescriptorImageInfo sample_zero_image_copy_info = {
      .sampler = sampler,
      .imageView = ms_image_copy_view,
      .imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
  };
  const VkWriteDescriptorSet sample_zero_write_descriptor_set[] = {
      {
          .sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
          .pNext = NULL,
          .dstSet = sample_zero_descriptor_sets[0],
          .dstBinding = 0,
          .dstArrayElement = 0,
          .descriptorCount = 1,
          .descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
          .pImageInfo = &sample_zero_image_info,
          .pBufferInfo = NULL,
          .pTexelBufferView = NULL,
      },
      {
          .sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
          .pNext = NULL,
          .dstSet = sample_zero_descriptor_sets[1],
          .dstBinding = 0,
          .dstArrayElement = 0,
          .descriptorCount = 1,
          .descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
          .pImageInfo = &sample_zero_image_copy_info,
          .pBufferInfo = NULL,
          .pTexelBufferView = NULL,
      },
  };
  vkUpdateDescriptorSets(device,
                         sizeof sample_zero_write_descriptor_set /
                             sizeof sample_zero_write_descriptor_set[0],
                         sample_zero_write_descriptor_set, 0, NULL);

  VkBuffer triangle_vertex_buffer = create_vertex_buffer(
      device, sizeof triangle / sizeof triangle[0], triangle);
  VkBuffer quad_vertex_buffer = create_vertex_buffer(
      device, sizeof full_rectangle / sizeof full_rectangle[0], full_rectangle);

  VkDeviceMemory resolved_buffer_memory;
  VkBuffer resolved_buffer =
      create_buffer(device, 64 * 64 * sizeof(struct RGBA8888),
                    VK_BUFFER_USAGE_TRANSFER_DST_BIT, &resolved_buffer_memory);
  VkDeviceMemory sample_zero_buffer_memory;
  /* We put both sample_zero_image and sample_zero_copy_image in the same
   * buffer */
  VkBuffer sample_zero_buffer = create_buffer(
      device, 2 * 64 * 64 * sizeof(struct RGBA8888),
      VK_BUFFER_USAGE_TRANSFER_DST_BIT, &sample_zero_buffer_memory);

  const VkCommandBufferBeginInfo cb_begin_info = {
      .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
      .pNext = NULL,
      .flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT,
      .pInheritanceInfo = NULL,
  };
  vkBeginCommandBuffer(cb, &cb_begin_info);

  initial_transition_image(cb, ms_image, VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT,
                           VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
                           VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL);
  begin_rendering(cb, ms_image_view);
  vkCmdBindPipeline(cb, VK_PIPELINE_BIND_POINT_GRAPHICS,
                    white_triangle_pipeline);
  vkCmdBindVertexBuffers(cb, 0, 1, &quad_vertex_buffer, &(VkDeviceSize){0});
  vkCmdDraw(cb, sizeof triangle / sizeof triangle[0], 1, 0, 0);
  vkCmdBindPipeline(cb, VK_PIPELINE_BIND_POINT_GRAPHICS, msaa_pipeline);
  vkCmdBindVertexBuffers(cb, 0, 1, &triangle_vertex_buffer, &(VkDeviceSize){0});
  vkCmdDraw(cb, sizeof triangle / sizeof triangle[0], 1, 0, 0);
  vkCmdEndRenderingKHR(cb);

  const VkImageMemoryBarrier ms_transfer_memory_barrier = {
      .sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
      .pNext = NULL,
      .srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT,
      .dstAccessMask = VK_ACCESS_TRANSFER_READ_BIT,
      .oldLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
      .newLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
      .srcQueueFamilyIndex = 0,
      .dstQueueFamilyIndex = 0,
      .image = ms_image,
      .subresourceRange = {
          .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
          .baseMipLevel = 0,
          .levelCount = 1,
          .baseArrayLayer = 0,
          .layerCount = 1,
      }};
  vkCmdPipelineBarrier(cb, VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
                       VK_PIPELINE_STAGE_TRANSFER_BIT, 0, 0, NULL, 0, NULL, 1,
                       &ms_transfer_memory_barrier);

  initial_transition_image(cb, ms_image_copy, VK_ACCESS_TRANSFER_WRITE_BIT,
                           VK_PIPELINE_STAGE_TRANSFER_BIT,
                           VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL);
  VkImageCopy image_copy = {
      .srcSubresource =
          {
              .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
              .mipLevel = 0,
              .baseArrayLayer = 0,
              .layerCount = 1,
          },
      .srcOffset =
          {
              .x = 0,
              .y = 0,
              .z = 0,
          },
      .dstSubresource =
          {
              .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
              .mipLevel = 0,
              .baseArrayLayer = 0,
              .layerCount = 1,
          },
      .dstOffset = {.x = 0, .y = 0, .z = 0},
      .extent =
          {
              .width = 64,
              .height = 64,
              .depth = 1,
          },
  };
  vkCmdCopyImage(cb, ms_image, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
                 ms_image_copy, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1,
                 &image_copy);
  const VkImageMemoryBarrier ms_image_copy_read_memory_barrier = {
      .sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
      .pNext = NULL,
      .srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT,
      .dstAccessMask = VK_ACCESS_SHADER_READ_BIT,
      .oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
      .newLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
      .srcQueueFamilyIndex = 0,
      .dstQueueFamilyIndex = 0,
      .image = ms_image_copy,
      .subresourceRange = {
          .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
          .baseMipLevel = 0,
          .levelCount = 1,
          .baseArrayLayer = 0,
          .layerCount = 1,
      }};
  vkCmdPipelineBarrier(cb, VK_PIPELINE_STAGE_TRANSFER_BIT,
                       VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT, 0, 0, NULL, 0,
                       NULL, 1, &ms_image_copy_read_memory_barrier);

  initial_transition_image(cb, resolved_image, VK_ACCESS_TRANSFER_WRITE_BIT,
                           VK_PIPELINE_STAGE_TRANSFER_BIT,
                           VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL);
  const VkImageResolve image_resolve = {
      .srcSubresource =
          {
              .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
              .mipLevel = 0,
              .baseArrayLayer = 0,
              .layerCount = 1,
          },
      .srcOffset =
          {
              .x = 0,
              .y = 0,
              .z = 0,
          },
      .dstSubresource =
          {
              .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
              .mipLevel = 0,
              .baseArrayLayer = 0,
              .layerCount = 1,
          },
      .dstOffset = {.x = 0, .y = 0, .z = 0},
      .extent = {
          .width = 64,
          .height = 64,
          .depth = 1,
      }};
  vkCmdResolveImage(cb, ms_image, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
                    resolved_image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1,
                    &image_resolve);

  const VkImageMemoryBarrier resolved_image_read_memory_barrier = {
      .sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
      .pNext = NULL,
      .srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT,
      .dstAccessMask = VK_ACCESS_TRANSFER_READ_BIT,
      .oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
      .newLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
      .srcQueueFamilyIndex = 0,
      .dstQueueFamilyIndex = 0,
      .image = resolved_image,
      .subresourceRange = {
          .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
          .baseMipLevel = 0,
          .levelCount = 1,
          .baseArrayLayer = 0,
          .layerCount = 1,
      }};
  vkCmdPipelineBarrier(cb, VK_PIPELINE_STAGE_TRANSFER_BIT,
                       VK_PIPELINE_STAGE_TRANSFER_BIT, 0, 0, NULL, 0, NULL, 1,
                       &resolved_image_read_memory_barrier);
  copy_image_to_buffer(cb, resolved_image, resolved_buffer, 0);

  const VkImageMemoryBarrier ms_to_sampled_memory_barrier = {
      .sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
      .pNext = NULL,
      .srcAccessMask = 0,
      .dstAccessMask = VK_ACCESS_SHADER_READ_BIT,
      .oldLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
      .newLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
      .srcQueueFamilyIndex = 0,
      .dstQueueFamilyIndex = 0,
      .image = ms_image,
      .subresourceRange = {
          .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
          .baseMipLevel = 0,
          .levelCount = 1,
          .baseArrayLayer = 0,
          .layerCount = 1,
      }};
  vkCmdPipelineBarrier(cb, VK_PIPELINE_STAGE_TRANSFER_BIT,
                       VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT, 0, 0, NULL, 0,
                       NULL, 1, &ms_to_sampled_memory_barrier);

  initial_transition_image(cb, sample_zero_image,
                           VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT,
                           VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
                           VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL);
  vkCmdBindPipeline(cb, VK_PIPELINE_BIND_POINT_GRAPHICS, sample_zero_pipeline);
  vkCmdBindVertexBuffers(cb, 0, 1, &quad_vertex_buffer, &(VkDeviceSize){0});
  begin_rendering(cb, sample_zero_image_view);
  vkCmdBindDescriptorSets(cb, VK_PIPELINE_BIND_POINT_GRAPHICS,
                          sample_zero_pipeline_layout, 0, 1,
                          &sample_zero_descriptor_sets[0], 0, NULL);
  vkCmdDraw(cb, sizeof full_rectangle / sizeof full_rectangle[0], 1, 0, 0);
  vkCmdEndRenderingKHR(cb);

  initial_transition_image(cb, sample_zero_copy_image,
                           VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT,
                           VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
                           VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL);
  begin_rendering(cb, sample_zero_copy_image_view);
  vkCmdBindDescriptorSets(cb, VK_PIPELINE_BIND_POINT_GRAPHICS,
                          sample_zero_pipeline_layout, 0, 1,
                          &sample_zero_descriptor_sets[1], 0, NULL);
  vkCmdDraw(cb, sizeof full_rectangle / sizeof full_rectangle[0], 1, 0, 0);
  vkCmdEndRenderingKHR(cb);

  const VkImageMemoryBarrier sample_zero_image_read_memory_barriers[] = {
      {.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
       .pNext = NULL,
       .srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT,
       .dstAccessMask = VK_ACCESS_TRANSFER_READ_BIT,
       .oldLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
       .newLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
       .srcQueueFamilyIndex = 0,
       .dstQueueFamilyIndex = 0,
       .image = sample_zero_image,
       .subresourceRange =
           {
               .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
               .baseMipLevel = 0,
               .levelCount = 1,
               .baseArrayLayer = 0,
               .layerCount = 1,
           }},
      {.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
       .pNext = NULL,
       .srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT,
       .dstAccessMask = VK_ACCESS_TRANSFER_READ_BIT,
       .oldLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
       .newLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
       .srcQueueFamilyIndex = 0,
       .dstQueueFamilyIndex = 0,
       .image = sample_zero_copy_image,
       .subresourceRange = {
           .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
           .baseMipLevel = 0,
           .levelCount = 1,
           .baseArrayLayer = 0,
           .layerCount = 1,
       }}};
  vkCmdPipelineBarrier(cb, VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
                       VK_PIPELINE_STAGE_TRANSFER_BIT, 0, 0, NULL, 0, NULL,
                       sizeof sample_zero_image_read_memory_barriers /
                           sizeof sample_zero_image_read_memory_barriers[0],
                       sample_zero_image_read_memory_barriers);
  copy_image_to_buffer(cb, sample_zero_image, sample_zero_buffer, 0);
  copy_image_to_buffer(cb, sample_zero_copy_image, sample_zero_buffer,
                       64 * 64 * sizeof(struct RGBA8888));

  const VkMemoryBarrier host_read_barrier = {
      .sType = VK_STRUCTURE_TYPE_MEMORY_BARRIER,
      .pNext = NULL,
      .srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT,
      .dstAccessMask = VK_ACCESS_HOST_READ_BIT,
  };
  vkCmdPipelineBarrier(cb, VK_PIPELINE_STAGE_TRANSFER_BIT,
                       VK_PIPELINE_STAGE_HOST_BIT, 0, 1, &host_read_barrier, 0,
                       NULL, 0, NULL);
  vkEndCommandBuffer(cb);

  const VkFenceCreateInfo fence_create_info = {
      .sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO, .pNext = NULL, .flags = 0};
  VkFence fence;
  vkCreateFence(device, &fence_create_info, NULL, &fence);

  const VkSubmitInfo submit_info = {
      .sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
      .pNext = NULL,
      .waitSemaphoreCount = 0,
      .pWaitSemaphores = NULL,
      .pWaitDstStageMask = NULL,
      .commandBufferCount = 1,
      .pCommandBuffers = &cb,
      .signalSemaphoreCount = 0,
      .pSignalSemaphores = NULL,
  };
  vkQueueSubmit(queue, 1, &submit_info, fence);
  vkWaitForFences(device, 1, &fence, VK_TRUE, UINT64_MAX);

  write_image_to_file(device, resolved_buffer_memory, 0, "resolved.png");
  write_image_to_file(device, sample_zero_buffer_memory, 0, "sample_zero.png");
  write_image_to_file(device, sample_zero_buffer_memory, 64 * 64,
                      "sample_zero_copy.png");
}
