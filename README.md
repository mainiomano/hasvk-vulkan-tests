# Hasvk-vulkan-tests
A collection of simple test/demo programs for testing hasvk on my HSW system.

## Building
``` sh
meson build
cd build
ninja
```

## Usage
The programs must be run in the build directory because of how they find their shader files.

### MSAA
``` sh
./msaa
```

The program renders an 8xMSAA test image and generates 3 files:

- `resolved.ppm` which is a resolved version of the image
- `sample_zero.ppm` which contains the sample zero of all pixels in the image
- `sample_zero_copy.ppm` which contains the sample zero of all pixels in a copy of the image
